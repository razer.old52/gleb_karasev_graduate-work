import React from 'react';
import style from './CartStyle.css'
import Card from '../Store/Cards/card.js';
import content from "../Store/Cards/content";
import item1 from '../Store/Img/1.png';
import item2 from '../Store/Img/2.png'
import item3 from '../Store/Img/3.png'
import {Button} from "react-bootstrap";
import StorePage from "../Store/StorePage";
import TotalPrice from "../Store/TotalPrice";
import {useSelector} from "react-redux";
import ProductsInCart from "./ProductsInCart";


const CartPage = () => {
    const items = useSelector( state => state.cart.itemsInCart);


    // if (items.length < 1) {
    //     return <h1 className='header_title'>Ваша Корзина пуста</h1>
    // }
    return (

        <div className='CartPage' >
            <div className='main_wrapper'>
                <header className='header_product'>
                    <h1 className='header_title'>Корзина с выбранными товарами</h1>
                </header>
                <main className='products_cart'>
                    {items.map( item => <ProductsInCart item={item} /> )}
                </main>
                <footer className='footer_product'>
                    <div className='footer_info'>
                        <span className='total_price'>Заказ на сумму: {TotalPrice(items)}</span>
                        <Button className='check_out'>Оформить заказ</Button>
                    </div>
                </footer>
            </div>
        </div>
    );
};

export default CartPage;