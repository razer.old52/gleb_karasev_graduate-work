import React from 'react';
import {useDispatch} from "react-redux";
import styles from './ProductsStyles.css'
import card from '../Store/Cards/card';
import ReducerComponent from "./Counter";
import DelFromCart from "./DelFromCart";

const ProductsInCart = ({item}) => {
    const dispatch = useDispatch();
    return (
        <div className='products_cart'>
            <div className='card_product'>
                <div className='card_img'>
                    <image src = {item.img} alt = 'product'/>
                </div>
                <div className='card_title'>
                    <span>{item.title}</span>
                </div>
                <div className='card_price'>{item.price}</div>
                <DelFromCart/>
                {/*<ReducerComponent/>*/}
            </div>
        </div>
    );
};

export default ProductsInCart;