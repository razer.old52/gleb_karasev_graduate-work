import React from 'react';
import {useDispatch} from "react-redux";
import {deleteItemFromCart} from "../../../redux/cart/reducer";
import {Button} from "react-bootstrap";

const DelFromCart = (card) => {
        const dispatch = useDispatch();
        const handleClick = () => {
            dispatch(deleteItemFromCart(card.id));
        }
    return (
        <div className='button'>
            <Button className='del_from_cart' onClick={handleClick}/>
        </div>
    );
};

export default DelFromCart;