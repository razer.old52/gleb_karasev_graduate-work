import React from 'react';
import {useSelector} from "react-redux";

const TotalPrice = () => {
    let totalArray
    const items = useSelector(state => state.cart.itemsInCart);
    return items.reduce((acc, card) => acc += +card.price, 0);
}

export default TotalPrice;