import React from 'react';
import {useDispatch} from "react-redux";
import style from './CardStyle.css';
import Content from "./content.js";
import {setItemsInCart} from "../../../../redux/cart/reducer";

const Card = (props) => {
    const dispatch = useDispatch();
    const handleClick = (e) => {
        e.stopPropagation();
        dispatch(setItemsInCart(props));
    }
    return (
        <div className='card'>
            <div className='card__preview'>
                <img src={props.img} alt='product' />
            </div>
            <h2 className='card__title'>{props.title}</h2>
            <div className='card__description'>{props.description}</div>
            <div className='card__footer'>
                <span className='price__item'>{props.price}Р/{props.weight}г</span>
                <button className='card__footer-button' onClick={handleClick}>+</button>
            </div>
        </div>
    );
};

export default Card;