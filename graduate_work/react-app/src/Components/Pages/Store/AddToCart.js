import React, {useCallback} from "react";
import {Link, useNavigate} from "react-router-dom";
import icon from "./Img/icon.png";


function AddToCart () {
    let navigate = useNavigate();
    function handleClick ()  {
        navigate ('/cart');
    }
    return (
        <div className='ButtonToCart'>
            <Link to='/cart' className='header__info-basket' onClick={handleClick}>
                <img src={icon} alt='Cart'/>
            </Link>
        </div>
    )
};

export default AddToCart;