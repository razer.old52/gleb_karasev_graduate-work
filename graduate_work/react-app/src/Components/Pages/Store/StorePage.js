import React, {useCallback} from 'react';
import style from './store.css'
import Card from "./Cards/card.js";
import {Link} from "react-router-dom";
import content from './Cards/content';
import icon from './Img/icon.png';
import {useSelector} from "react-redux";
import TotalPrice from "./TotalPrice";
import AddToCart from "./AddToCart";


export const StorePage = () => {
    return (
        <div className='StorePage'>
            <div className='main__wrapper'>
                <header className='header'>
                    <h1 className='header__title'><Link to='/' className='header__title-link' >Наша продукция</Link></h1>
                    <div className='header__info'>
                        <AddToCart/>
                        <span className='TotalPrice'>{TotalPrice()}</span>
                    </div>
                </header>
                <main className='products'>
                        {content.map(item => (
                            <Card
                                img={item.img}
                                title={item.title}
                                description={item.description}
                                price={item.price}
                                weight={item.weight}
                            />
                            ))}
                </main>
            </div>
        </div>
    );
};

export default StorePage;
