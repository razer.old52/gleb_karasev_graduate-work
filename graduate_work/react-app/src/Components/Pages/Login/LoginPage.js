import React from 'react';
import style from './login.css';
// import script from './script'


const LoginPage = () => {
    return (
        <div className='LoginPage'>
            <div className='popup-container'>
                <div className='popup'>
                    <h1 className='popup__title'>Вход</h1>
                    <form className='popup__form'>
                        <div className='popup__textblock' id='mail'>
                            <label htmlFor='mail-content' className='popup__textblock-label'></label>
                            <div className='popup__formraw'>
                                <div className='popup__formblock-interactive'>
                                    <input type='text' className='popup__textblock-textfield' placeholder='E-mail'
                                           autoComplete='username' id='mail-content'/>
                                        <div className='popup__warning'>
                                            Поле обязательно для заполнения
                                        </div>
                                </div>
                            </div>
                        </div>
                        <div className='popup__textblock'>
                            <label htmlFor='password-content' className='popup__textblock-label'></label>
                            <div className='popup__formraw'>
                                <div className='popup__formblock-interactive'>
                                    <input type='password' id='password-content' className='popup__textblock-textfield'
                                           placeholder='Пароль' autoComplete='current-password'/>
                                        <div className='popup__warning' id='password-warning'>
                                            Поле обязательно для заполнения
                                        </div>
                                </div>
                            </div>
                        </div>
                        <div className='popup__checkbox' id='agreement'>
                            <div className='popup__formblock-interactive'>
                                <div className='popup__checkbox-raw'>
                                    <input type='checkbox' id='agreement-check' className='popup__checkbox-wrapped'/>
                                        <div className='popup__checkbox-point'></div>
                                        <label htmlFor='agreement-check' className='popup__checkbox-label'>
                                            <span>Я согласен получать обновления на почту</span>
                                        </label>
                                </div>
                                <div className='popup__warning' id='agreement-warning'>
                                    Поле обязательно для заполнения
                                </div>
                            </div>
                        </div>
                        <div className='popup__button-container'>
                            <button className='popup__button' id='registrationSubmit'>Войти</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    );
};

export default LoginPage;