import logo from './logo.svg';
import './App.css';
import {Outlet, Route, Routes} from 'react-router-dom';
import {Provider} from "react-redux";
import {store} from "./redux";
import StorePage from './Components/Pages/Store/StorePage';
import CartPage from './Components/Pages/Cart/CartPage';
import LoginPage from './Components/Pages/Login/LoginPage';



function App() {
  return (
     <Provider store={store} >
      <Routes>
          <Route path={'/'} element={<StorePage/>}/>
          <Route path={'cart'} element={<CartPage/>}/>
            <Route path={'login'} element={<LoginPage/>}/>
      </Routes>
     </Provider>
  );
}
function Layout() {
    return (
        <div className='App'>
            <header/>
            <main className={'content'}>
                <Outlet/>
            </main>
            <footer/>
        </div>
    )
}


export default App;
